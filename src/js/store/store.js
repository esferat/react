import {createStore} from 'redux'
import {rootReducer} from '../reducer/RootReducer'

export default function configureStore(initialState) {
    return createStore(rootReducer, initialState);
}

