import {combineReducers} from "redux";
import {details} from "./DetailsReducer";
import {loading} from "./LoadingReducer";

export const rootReducer = combineReducers({
    loading,
    details
});