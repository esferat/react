import {
    ADD_DATA,
    FILTER_BY_ID,
    FILTER_BY_NAME,
    RESET_FILTER,
    TOGGLE_ACTIVE,
    UPDATE_RECORD
} from "../constants/Constants";

let gridState = {
    records: [],
    filtered: []
};

export function details(state = gridState, action) {
    switch (action.type) {
        case ADD_DATA: {
            return {
                ...state,
                records: [...action.value],
                filtered: [...action.value]
            };
        }
        case RESET_FILTER: {
            return {
                ...state,
                filtered: [...state.records]
            }
        }
        case TOGGLE_ACTIVE: {
            let newRecords = [...state.records];
            let newFiltered = [...state.filtered];
            let recordIndex = newRecords.findIndex(record => record.id === action.value);
            let filteredRecordIndex = newFiltered.findIndex(record => record.id === action.value);

            newRecords[recordIndex] = {
                ...newRecords[recordIndex],
                active: !newRecords[recordIndex].active
            };
            if (filteredRecordIndex !== undefined) {
                newFiltered[filteredRecordIndex] = {
                    ...newFiltered[filteredRecordIndex],
                    active: !newFiltered[filteredRecordIndex].active
                };
            }
            return {...state, records: newRecords, filtered: newFiltered};
        }
        case FILTER_BY_NAME: {
            return {
                ...state,
                filtered: state.records.filter((record) =>
                    record.firstName.toUpperCase().includes(action.value.toUpperCase())
                )
            };
        }
        case FILTER_BY_ID: {
            if (action.value) {
                return {
                    ...state,
                    filtered: state.records.filter(record =>
                        record.id === Number(action.value)
                    )
                };
            } else {
                return state
            }
        }
        case UPDATE_RECORD: {
            let newRecords = [...state.records];
            let newFiltered = [...state.filtered];

            let recordIndex = newRecords.findIndex(record => record.id === action.details.id);
            let filteredRecordIndex = newFiltered.findIndex(record => record.id === action.details.id);

            newRecords[recordIndex] = action.details;
            if (filteredRecordIndex) {
                newFiltered[filteredRecordIndex] = action.details;
            }

            return {
                ...state,
                records: newRecords,
                filtered: newFiltered
            };
        }
        default:
            return state
    }
}