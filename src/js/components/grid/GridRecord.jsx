import React from "react";
import * as PropTypes from 'prop-types';
import {Edit} from "@material-ui/icons";
import {Link} from "react-router-dom";


export default function GridRecord({record, toggleActive, index}) {

    return (
        <tr>
            <td>
                <Link to = {`/details/${record.id}`}>
                    {record.id}
                </Link>
            </td>
            <td>
                {record.firstName}
            </td>
            <td>
                {record.lastName}
            </td>
            <td className = {'text-centered'}>
                <div className = "custom-control custom-checkbox">
                    <input className = {'custom-control-input'}
                           readOnly = {true}
                           type = {'checkbox'}
                           checked = {record.active}
                           onClick = {toggleActive}
                           id = {`checkbox__${index}`}/>
                    <label className = {'custom-control-label'} htmlFor = {`checkbox__${index}`}/>
                </div>
            </td>
            <td className = {'text-centered'}>
                <Link to = {`edit/${record.id}`}><Edit/></Link>
            </td>
        </tr>
    )
}

GridRecord.propTypes = {
    record: PropTypes.shape({
        id: PropTypes.number,
        firstName: PropTypes.string,
        lastName: PropTypes.string,
        active: PropTypes.bool
    }),
    toggleActive: PropTypes.func,
    index: PropTypes.number,
};