import React from "react";
import {connect} from 'react-redux'
import GridRecord from "./GridRecord";
import {addData, filteredRecords, resetFilter, toggleActive} from "../../actions/DetailsActions";
import * as PropTypes from 'prop-types';
import ReceiveData from "../../server/server";
import CircularProgress from "@material-ui/core/CircularProgress";
import {startLoading, stopLoading} from "../../actions/LoadingActions";


function GridComponent(props) {

    const [filter, setFilter] = React.useState('');

    let inputRef = React.createRef();

    React.useEffect(() => {
        if (props.loading && inputRef.current) {
            inputRef.current.focus();
        }
    }, [inputRef, props.loading]);

    React.useEffect(() => {

        if (props.records.length === 0) {
            props.startLoadingAction();
            ReceiveData()
                .then(json => props.addDataAction(json))
                .then(() => props.stopLoadingAction());
        }

        return () => props.resetFilterAction();
    }, []);

    const toggleActive = (id) => {
        props.toggleActiveAction(id);
    };

    const handleFilterChange = (event) => {
        props.filteredRecordsAction(event.target.value);
        setFilter(event.target.value);
    };

    if (props.loading) {
        return (
            <div className = {'main'}>
                <div className = {'spinner'}>
                    <CircularProgress/>
                </div>
            </div>
        )
    } else {
        return (
            <div className = {'main'}>
                <input className = {'input'}
                       placeholder = {'Filter by...'}
                       ref = {inputRef}
                       value = {filter}
                       onChange = {handleFilterChange}/>
                <table className = "table table-condensed">
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th>FirstName</th>
                        <th>LastName</th>
                        <th>Active</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    {
                        props.filtered.map(
                            (record, index) => {
                                return (
                                    <GridRecord record = {record}
                                                key = {index}
                                                index = {index}
                                                toggleActive = {() => toggleActive(record.id)}/>
                                )
                            }
                        )
                    }
                    </tbody>
                </table>
            </div>
        )
    }
}

GridComponent.propTypes = {
    records: PropTypes.array.isRequired,
    filtered: PropTypes.array.isRequired,
    loading: PropTypes.bool.isRequired
};

export default connect(
    state => ({
        records: state.details.records,
        filtered: state.details.filtered,
        loading: state.loading
    }),
    dispatch => ({
        toggleActiveAction: (params) => dispatch(toggleActive(params)),
        startLoadingAction: () => dispatch(startLoading()),
        stopLoadingAction: () => dispatch(stopLoading()),
        addDataAction: (params) => dispatch(addData(params)),
        resetFilterAction: () => dispatch(resetFilter()),
        filteredRecordsAction: (value) => dispatch(filteredRecords(value))
    })
)(GridComponent);