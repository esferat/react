import React from "react";
import * as PropTypes from 'prop-types';
import TextField from "@material-ui/core/TextField";


export default function InputComponent(props) {

    let [value, setValue] = React.useState(props.value ? props.value : '');

    if (props.value && value !== props.value) {
        setValue(props.value)
    }

    const onChange = (event) => {
        setValue(event.target.value);
        props.onChange(event);
    };

    if (props.isDisplayed) {
        return (
            <TextField id = {props.id}
                       onChange = {onChange}
                       onKeyPress = {props.onKeyPress}
                       placeholder = {props.placeholder}
                       label = {`${props.label}`}
                       disabled = {props.readOnly}
                       required = {props.required}
                       helperText = {props.helperText}
                       value = {value}
                       fullWidth = {true}
                       size = {'small'}
                       error = {!props.isValid}
                       variant = {'outlined'}
            />
        )
    } else {
        return []
    }
}

InputComponent.propTypes = {
    isValid: PropTypes.bool,
    readOnly: PropTypes.bool,
    required: PropTypes.bool,
    placeholder: PropTypes.string,
    helperText: PropTypes.string,
    id: PropTypes.string,
    onKeyPress: PropTypes.func,
    label: PropTypes.string,
    isDisplayed: PropTypes.bool
};

InputComponent.defaultProps = {
    isValid: true,
    readOnly: false,
    required: false,
    isDisplayed: true,
    onKeyPress: () => {
    },
    onChange: () => {
    },
    helperText: ''
};