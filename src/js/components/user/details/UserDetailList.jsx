import React from "react";
import {connect} from 'react-redux'
import UserDetail from "./UserDetail";
import ReceiveData from "../../../server/server";
import CircularProgress from "@material-ui/core/CircularProgress";
import {startLoading, stopLoading} from "../../../actions/LoadingActions";
import {addData, filterById, resetFilter} from "../../../actions/DetailsActions";


function UserDetailList(props) {

    React.useEffect(() => {
        if (props.records.length === 0) {
            props.startLoadingAction();
            ReceiveData()
                .then(json => props.addDataAction(json))
                .then(() => props.filterByIdAction(props.match.params.id))
                .then(() => props.stopLoadingAction());
        } else {
            props.startLoadingAction();
            props.filterByIdAction(props.match.params.id);
            props.stopLoadingAction();
        }

        return () => props.resetFilterAction();
    }, [props.match.params.id]);

    if (props.loading) {
        return (
            <div className = {'container'}>
                <div className = {'spinner'}>
                    <CircularProgress/>
                </div>
            </div>
        )
    } else {
        return (
            <div className = "container">
                <div className = "row">
                    {
                        props.filtered.map((record) => (
                            <UserDetail key = {record.id}
                                        user = {record}
                                        record = {props.records.find(user => user.id === record.id)}/>
                        ))
                    }
                </div>
            </div>
        )
    }
}

export default connect(
    state => ({
        records: state.details.records,
        filtered: state.details.filtered,
        loading: state.loading
    }),
    dispatch => ({
        startLoadingAction: () => dispatch(startLoading()),
        stopLoadingAction: () => dispatch(stopLoading()),
        addDataAction: (params) => dispatch(addData(params)),
        resetFilterAction: () => dispatch(resetFilter()),
        filterByIdAction: (value) => dispatch(filterById(value))
    })
)(UserDetailList);