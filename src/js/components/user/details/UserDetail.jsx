import React from "react";
import avatar from '../../../../img/avatar.svg'


export default function UserDetail(props) {

    return (
        <div className = "col-md-offset-2 col-md-8 col-lg-offset-3 col-lg-6">
            <div className = "well profile">
                <div className = "col-sm-12">
                    <div className = "col-xs-12 col-sm-4 text-center">
                        <figure>
                            <img
                                src = {avatar}
                                alt = ""
                                className = "img-circle img-responsive"
                                width = {100}
                                height = {100}
                            />
                        </figure>
                    </div>
                    <div className = "col-xs-12 col-sm-8">
                        <h2>{props.record.firstName} {props.record.lastName}</h2>
                        <p><strong>About: </strong>{props.user.about}</p>
                        <p><strong>Hobbies: </strong>{props.user.hobby}</p>
                        <p><strong>Skills: </strong></p>
                        <ul>
                            {
                                props.user.skills.map((skill, index) => (
                                    <li key = {index}>{skill}</li>
                                ))
                            }
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    )
}