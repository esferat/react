import React from "react";
import Button from "@material-ui/core/Button";
import {Save} from "@material-ui/icons";
import * as PropTypes from 'prop-types';


export default function UserEditNavigation({handleSaveClick, handleCancelClick, currentId, idList}) {

    const hasPrevious = () => {
        if (currentId) {
            let index = idList.findIndex(id => id === Number(currentId));
            return index > 0 && idList.length > 1;
        }
        return false;
    };

    const hasNext = () => {
        if (currentId) {
            let index = idList.findIndex(id => id === Number(currentId));
            return index < idList.length - 1 && idList.length > 1;
        }
        return false;
    };

    return (
        <div className = {'navigation'}>
            <Button color = {'primary'}
                    variant = {'outlined'}
                    onClick = {handleCancelClick}>
                <Save/> Cancel
            </Button>
            <Button color = {'primary'}
                    variant = {'outlined'}
                    onClick = {handleSaveClick}>
                <Save/> Save
            </Button>
            <Button color = {'primary'}
                    variant = {'outlined'}
                    disabled = {!hasPrevious()}
                    href = {`#/edit/${Number(currentId) - 1}`}>
                <Save/> Previous
            </Button>
            <Button color = {'primary'}
                    variant = {'outlined'}
                    disabled = {!hasNext()}
                    href = {`#/edit/${Number(currentId) + 1}`}>
                <Save/> Next
            </Button>
        </div>
    )
}

UserEditNavigation.propTypes = {
    handleCancelClick: PropTypes.func.isRequired,
    handleSaveClick: PropTypes.func.isRequired,
    currentId: PropTypes.string.isRequired,
    idList: PropTypes.arrayOf(PropTypes.number).isRequired
};