export default class Message {

    constructor() {
    }

    setTitle(title) {
        this.title = title
    };

    createSuccessMessage(bodyMessage, onConfirmClick) {
        this.title = 'Успех';
        this.confirmButtonTitle = 'Ok';
        this.theme = 'success';
        this.bodyMessage = bodyMessage;
        this.onConfirmClick = onConfirmClick;
        return this.getMessageObject();
    }

    createWarningMessage(bodyMessage) {
        this.title = 'Внимание';
        this.confirmButtonTitle = 'Ok';
        this.theme = 'warning';
        this.bodyMessage = bodyMessage;

        return this.getMessageObject();
    }

    setBodyMessage(bodyMessage) {
        this.bodyMessage = bodyMessage;
    };

    setSuccessTheme() {
        this.theme = 'success';
    }

    setWarningTheme() {
        this.theme = 'warning';
    }

    setInfoTheme() {
        this.theme = 'info';
    }

    setConfirmButtonTitle(confirmButtonTitle) {
        this.confirmButtonTitle = confirmButtonTitle;
    }

    setDeclineButtonTitle(declineButtonTitle) {
        this.declineButtonTitle = declineButtonTitle;
    }

    setOnConfirmClick(onConfirmClick) {
        this.onConfirmClick = onConfirmClick
    }

    getMessageObject() {
        return {
            title: this.title,
            bodyMessage: this.bodyMessage,
            theme: this.theme,
            confirmButtonTitle: this.confirmButtonTitle,
            declineButtonTitle: this.declineButtonTitle,
            onConfirmClick: this.onConfirmClick,
            canBeClosed: this.canBeClosed !== undefined ? this.canBeClosed : true
        }
    }
}
