import React from 'react';
import avatar from "../../../../img/avatar.svg";
import {connect} from "react-redux";
import InputComponent from "../../InputComponent";
import {updateDetails} from "../../../actions/DetailsActions";
import UserEditNavigation from "./UserEditNavigation";
import PopupMessage from "./PopupMessage";
import Message from "./Message";

function UserEdit(props) {

    const [currentDetails, setCurrentDetails] = React.useState({});
    const [firstName, setFirstName] = React.useState('');
    const [lastName, setLastName] = React.useState('');
    const [about, setAbout] = React.useState('');
    const [hobby, setHobby] = React.useState('');
    const [skills, setSkills] = React.useState([]);
    const [showMessage, setShowMessage] = React.useState(false);
    const [message, setMessage] = React.useState({});

    const [isChanged, setIsChanged] = React.useState(false);

    React.useEffect(() => {
        if (props.records.length > 0) {
            let currentDetails = props.records.find(detail => detail.id === Number(props.match.params.id));
            setCurrentDetails(currentDetails);
            setFirstName(currentDetails.firstName);
            setLastName(currentDetails.lastName);
            setAbout(currentDetails.about);
            setHobby(currentDetails.hobby);
            setSkills(currentDetails.skills);
        }
    }, [props.match.params.id]);

    const calculateIsChanged = () => {
        setIsChanged(
            firstName !== currentDetails.firstName ||
            lastName !== currentDetails.lastName ||
            about !== currentDetails.about ||
            hobby !== currentDetails.hobby
        );
    };

    const handleFirstNameChange = (event) => {
        setFirstName(event.target.value);
        calculateIsChanged();
    };

    const handleLastNameChange = (event) => {
        setLastName(event.target.value);
        calculateIsChanged();
    };

    const handleAboutChange = (event) => {
        setAbout(event.target.value);
        calculateIsChanged();
    };

    const handleHobbyChange = (event) => {
        setHobby(event.target.value);
        calculateIsChanged();
    };

    const showPopupMessage = (message) => {
        setMessage(message);
        setShowMessage(true);
    };

    const handleCancelButtonClick = () => {
        if (isChanged) {
            let message = new Message();
            message.setTitle("Reject all changes?");
            message.setConfirmButtonTitle("Yes");
            message.setDeclineButtonTitle("No");
            message.setOnConfirmClick(() => window.location.assign("#/grid"));
            message.setWarningTheme();
            showPopupMessage(message);
        } else {
            window.location.assign("#/grid")
        }
    };

    const handleButtonClick = () => {
        props.updateDetailsAction({
            ...currentDetails,
            firstName: firstName,
            lastName: lastName,
            about: about,
            hobby: hobby,
            skills: skills
        });

        setIsChanged(false);
    };

    if (props.records.length > 0) {
        return (
            <div className = "col-md-offset-2 col-md-8 col-lg-offset-3 col-lg-6">
                <PopupMessage title = {message.title}
                              bodyMessage = {message.bodyMessage}
                              theme = {message.theme}
                              shouldShowPopup = {showMessage}
                              confirmButtonTitle = {message.confirmButtonTitle}
                              declineButtonTitle = {message.declineButtonTitle}
                              onConfirmClick = {message.onConfirmClick}
                              canBeClosed = {message.canBeClosed}
                              onClose = {() => setShowMessage(false)}/>
                <div className = "well profile col-sm-12">
                    <div className = "col-xs-12 col-sm-4 text-center">
                        <figure>
                            <img
                                src = {avatar}
                                alt = ""
                                className = "img-circle img-responsive"
                                width = {100}
                                height = {100}
                            />
                        </figure>
                    </div>
                    <div className = "col-xs-12 col-sm-8 profile-parameters">
                        <InputComponent value = {firstName}
                                        placeholder = {'Type firstName'}
                                        label = {'FirstName'}
                                        onChange = {handleFirstNameChange}/>
                        <InputComponent value = {lastName}
                                        placeholder = {'Type lastName'}
                                        label = {'LastName'}
                                        onChange = {handleLastNameChange}/>
                        <InputComponent value = {about}
                                        placeholder = {'Type hobby'}
                                        label = {'About'}
                                        onChange = {handleAboutChange}/>
                        <InputComponent value = {hobby}
                                        placeholder = {'Type hobby'}
                                        label = {'Hobby'}
                                        onChange = {handleHobbyChange}/>

                        <p><strong>Skills: </strong></p>
                        <ul>
                            {
                                skills.map((skill, index) => (
                                    <li key = {index}>{skill}</li>
                                ))
                            }
                        </ul>
                    </div>
                    <UserEditNavigation currentId = {props.match.params.id}
                                        idList = {props.records.map(record => record.id)}
                                        handleCancelClick = {handleCancelButtonClick}
                                        handleSaveClick = {handleButtonClick}/>
                </div>
            </div>
        )
    } else {
        return (
            <div className = "col-md-offset-2 col-md-8 col-lg-offset-3 col-lg-6">
                <div> User data is not loaded</div>
            </div>
        )
    }
}

export default connect(
    state => ({
        records: state.details.records
    }),
    dispatch => ({
        updateDetailsAction: (detail) => dispatch(updateDetails(detail)),
    })
)(UserEdit);