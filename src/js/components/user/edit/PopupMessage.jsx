import React, {Component} from 'react';
import * as PropTypes from 'prop-types';
import Button from "@material-ui/core/Button";
import './popup-message.css';

export default class PopupMessage extends Component {

    static propTypes = {
        title: PropTypes.string.isRequired,
        bodyMessage: PropTypes.string,
        theme: PropTypes.oneOf(["info", "success", "warning", "error"]),
        confirmButtonTitle: PropTypes.string,
        declineButtonTitle: PropTypes.string,
        shouldShowPopup: PropTypes.bool.isRequired,
        onDeclineClick: PropTypes.func,
        onConfirmClick: PropTypes.func,
        canBeClosed: PropTypes.bool,
        onClose: PropTypes.func.isRequired,
    };

    static defaultProps = {
        title: '',
        shouldShowPopup: false,
        theme: 'info',
        canBeClosed: true
    };

    render() {
        if (this.props.shouldShowPopup) {
            return (
                <div className = "popup-dialog" onClick = {this.props.canBeClosed === false ? () => {
                } : this.props.onClose}>
                    <div className = {`popup-dialog__window`}>
                        <div onClick = {this.props.canBeClosed === false ? () => {
                        } : this.props.onClose} className = {`close ${this.props.theme}`}/>
                        <div className = {'popup-dialog__container'}>
                            <div className = 'popup-dialog__header'>
                                {
                                    this.props.title &&
                                    <div
                                        className = {`popup-dialog__title ${this.props.theme}`}>{this.props.title}</div>
                                }
                            </div>
                            <div className = 'popup-dialog__body'>
                                {
                                    this.props.bodyMessage &&
                                    <div className = 'popup-dialog__sub-title'>{this.props.bodyMessage}</div>
                                }
                            </div>
                        </div>
                        <div className = 'popup-dialog__buttons'>
                            {
                                this.props.confirmButtonTitle &&
                                <div className = {`popup-dialog__button ${this.props.theme}`}>
                                    <Button color = {'primary'}
                                            variant = {'outlined'}
                                            onClick = {this.props.onConfirmClick}>
                                        {this.props.confirmButtonTitle}
                                    </Button>
                                </div>
                            }
                            {
                                this.props.declineButtonTitle &&
                                <div className = {`popup-dialog__button ${this.props.theme}`}>
                                    <Button color = {'primary'}
                                            variant = {'outlined'}
                                            onClick = {this.props.onDeclineClick}>
                                        {this.props.declineButtonTitle}
                                    </Button>
                                </div>
                            }
                        </div>
                    </div>
                </div>
            )
        }
        return null;
    }
}
