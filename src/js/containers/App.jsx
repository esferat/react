import React from 'react';
import {Link} from "react-router-dom";


export default function App({children}) {
    return (
        <div>
            <h1>Our awesome app</h1>
            <ul role = "nav">
                <li><Link to = "/grid">Grid</Link></li>
                <li><Link to = "/details">Details</Link></li>
            </ul>
            {children}
        </div>
    )
};

