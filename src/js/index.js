import React from "react";
import ReactDOM from 'react-dom';
import {HashRouter, Route, Switch} from "react-router-dom";
import {Provider} from 'react-redux'
import App from "./containers/App";
import GridComponent from "./components/grid/GridComponent";
import UserDetailList from "./components/user/details/UserDetailList";
import configureStore from './store/store';
import UserEdit from "./components/user/edit/UserEdit";

import "bootstrap/dist/css/bootstrap.css";
import '../style/main.css';


const store = configureStore();

ReactDOM.render(
    <Provider store = {store}>
        <HashRouter>
            <App/>
            <Switch>
                <Route path = {"/grid"} component = {GridComponent}/>
                <Route path = {"/edit/:id"} component = {UserEdit}/>
                <Route exact path = {"/details"} component = {UserDetailList}/>
                <Route path = {"/details/:id"} component = {UserDetailList}/>
            </Switch>
        </HashRouter>
    </Provider>,
    document.getElementById('app')
);