let gridRecords = [
    {
        firstName: "John",
        lastName: "Doe",
        active: false,
        about: "Nice guy",
        hobby: "Likes drinking wine",
        skills: ["html", "javascript", "redux"],
        id: 1
    },
    {
        firstName: "Mary",
        lastName: "Moe",
        active: false,
        about: "Cute girl",
        hobby: "Likes playing xbox whole days long",
        skills: ["Fortran", "Lua", "R#"],
        id: 2
    },
    {
        firstName: "Peter",
        lastName: "Noname",
        active: true,
        about: "Awesome Developer",
        hobby: "He is the author of React.js",
        skills: ["Lisp", "Om", "JS"],
        id: 3
    }
];

export default function ReceiveData() {
    return new Promise(resolve => {
            setTimeout(() => {
                resolve(gridRecords)
            }, 2000)
        }
    );
};