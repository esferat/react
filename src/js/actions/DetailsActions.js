import * as Types from "../constants/Constants";

export function toggleActive(index) {
    return {
        type: Types.TOGGLE_ACTIVE,
        value: index
    };
}

export function filteredRecords(value) {
    return {
        type: Types.FILTER_BY_NAME,
        value: value
    };
}

export function updateDetails(details) {
    return {
        type: Types.UPDATE_RECORD,
        details
    };
}

export function filterById(id) {
    return {
        type: Types.FILTER_BY_ID,
        value: id
    };
}

export function resetFilter() {
    return {
        type: Types.RESET_FILTER,
    }
}

export function addData(value) {
    return {
        type: Types.ADD_DATA,
        value
    }
}
