import * as Types from "../constants/Constants";

export function startLoading() {
    return {
        type: Types.START_LOADING
    }
}

export function stopLoading() {
    return {
        type: Types.STOP_LOADING
    }
}