const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const PATHS = {
    index: "./src/js/index",
    build: path.join(__dirname, 'build')
};

module.exports = {
    entry: {
        index: PATHS.index
    },
    module: {
        rules: [
            {
                test: /.(js|jsx)?$/,
                exclude: /node_modules/,
                use: [{
                    loader: 'babel-loader',
                    options: {
                        presets: [
                            '@babel/preset-env',
                            '@babel/preset-react'
                        ],
                        sourceMap: true,
                        plugins: [
                            "@babel/plugin-proposal-class-properties"
                        ]
                    }
                },
                    {
                        loader: "source-map-loader"
                    }
                ]
            },
            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader']
            },
            {
                test: /\.html$/,
                use: ['html-loader']
            },
            {
                test: /\.(png|woff|woff2|eot|ttf|svg)$/,
                use: 'url-loader?limit=100000'
            }
        ]
    },
    resolve: {
        extensions: ['.js', '.jsx', '.css']
    },
    plugins: [
        new HtmlWebpackPlugin({
            title: 'Awesome app',
            favicon: './src/img/favicon.png',
            template: path.join(__dirname, 'public/index.html')
        }),
    ],
    output: {
        path: PATHS.build,
        filename: 'bundle.js'
    }
};