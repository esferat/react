const express = require('express');
const path = require('path');
const app = express();

app.use('/ext',express.static(path.join(__dirname, 'build')));

app.get('/ext/*', function (req, res) {
    res.sendFile(path.join(__dirname, 'build', 'index.html'));
});

app.get('/api/hello', function (req, res) {
    res.json({
        message: "hello!!"
    });
});

const PORT = process.env.PORT || 5000;
app.listen(PORT);